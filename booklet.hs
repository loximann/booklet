import Data.List

import System.Environment
import System.FilePath

collate :: [[a]] -> [a]
collate [] = []
collate ll = case heads of Just heads' -> heads' ++ (collate $ map tail ll)
                           Nothing     -> []
                where heads = sequence $ map head' ll
                      head' [] = Nothing
                      head' x  = Just $ head x

data Page = Page Int | Blank deriving Show

instance Eq Page where
  Blank == Blank = True
  (Page _) == (Page _) = True
  _ == _ = False

pages n = map fromPageNumber $ collate [ [n',(n'-2)..],
                                         [1,3..(n'`div`2)],
                                         [2,4..],
                                         [(n'-1),(n'-3)..] ]
            where n' = 4 * (ceiling ((fromIntegral n) / 4))
                  fromPageNumber x = if (x<=n) then Page x else Blank

escape [] = []
escape (' ':xx) = '\\':' ':(escape xx)
escape (x:xx) = x:(escape xx)

qpdf :: String -> [[Page]] -> String
qpdf filename pages = unwords $ map escape $ ("qpdf":filename:"--pages":args) ++
                                ("--":[underscore filename "booklet"])
                        where args = argify 0 pages
                              argify :: (Num a, Show a) =>  a -> [[Page]] -> [String]
                              argify _ [] = []
                              argify n (head:ll) =
                                  case head of []        -> argify n ll
                                               (Blank:l) -> ("blank_" ++ (show (n+1)) ++ ".pdf"):(argify (n+1) (l:ll))
                              -- The only possibility is a list of [Page x]
                                               otherwise -> these++(argify n ll) where
                                                                these = filename:[concat $ intersperse "," [show pp | (Page pp) <- head]]
                              underscore filename suffix = concat $ (dropExtension filename):"_":suffix:[takeExtension filename]

main = do
    (filename:npages:_) <- getArgs
    putStrLn $ qpdf filename $ group $ pages (read npages)
